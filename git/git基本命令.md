- **专有名词**
    ![工作原理](\images\18087435-2b52aaf65be47442.webp)
  - Workspace	工作区
  - Index / Stage 暂存区
  - Repository     仓库区
  - Remote           远程仓库

  ```txt
  1.forking 	派生
  2.check out	签出
  3.stash		储藏
  4.pull		抓取
  5.fetch		拉取
  6.merge		合并
  7.branch	分支
  8.tracked	跟踪
  9.push		推送
  ```
  ![基本命令](\images\18087435-bf2a996ef50a21b0.webp)

- **创建仓库**

  - `git init`

    在当前目录新建一个git仓库，执行完毕后当前目录 会出现 .git 目录（默认隐藏）;

  - `git init [project-name]`

    新建一个目录，并将其初始化为Git仓库

  - `git clone [url]`

    克隆一个远程仓库到本地

  

- **配置**

  - `git config --list`

    显示当前git配置

  - `git config -e [--global]`

    编辑Git配置文件

  - `git config [--global] user.name "[name]"`

    `git config [--global] user.email "[email address]"`

    设置用户信息

  - git 修改当前的project的用户名的目录

    `git config user.name 目标用户名；`

  - git 修改当前的project提交邮箱的命令

    `git config user.email  目标邮箱名;`

  - 全局修改

    `git config --global user.name 目标用户名`

    `git config --global user.email 目标邮箱名`

- **添加/删除文件**

  - `git add [file1] [file2] ...`

    添加指定文件到暂存区

  - `git add [dir]`

    添加指定目录到暂存区，包括子目录

  - `git add .`

    添加当前目录的所有文件到暂存区

  - `git add -p`

    添加每个变化前，等会要去确认，对于同一个文件的到处变化，可以实现分次提交

  - `git rm [file1] [file2] ...`

    删除工作区文件，并将这次删除放入暂存区

  - `git rm --cached [file]`

    停止追踪指定文件，但文件会保留在工作区

  - `git mv [file-original] [file-rename]`

    改名文件，并将这个改名放入暂存区

- **代码提交**

  - `git commit -m ['message']`

    从暂存区提交到仓库区

  - `git commit [file1] [file2] ... -m ['message']`

    提交暂存区的指定文件到仓库区

  - `git commit -a`

    提交工作区自上次commit之后的变化，直接到仓库区

  - `git commit -v` 

    提交时显示所有diff信息

  - `git commit --amend -m [message]`

    使用一次新的commit，代替上一次提交，如果代码没有任何新变化，则用来改写上一次commit的提交信息

  - `git commit --amend [file1] [file2] ...`

    重做上一次commit，并包括指定文件的新变化

- **分支**

  - `git branch`

    列出所有本地分支

  - `git branch -r`

    列出所有远程分支

  - `git branch -a`

    列出所有本地和远程分支

  - `git branch [branch-name]`

    新建一个分支，但依然停留在当前分支

  - `git checkout -b [branch]`

    `git checkout -b appoint_box(别名) origin/feature/20200324_appoint_box_1(分支名)`

    新建一个分支，并切换到该分支

  - `git branch [branch] [commit]`

    新建一个分支，指向指定commit

  - `git branch --track [branch] [remote-branch]`

    新建一个分支，与指定的远程分支建立追踪关系

  - `git checkout [branch-name]`

    切换到指定分支，并更新工作区

  - `git checkout -`

    切换到上一个分支

  - `git branch --set-upstream [branch] [remote-branch]`

    建立追踪关系，在现有分支与指定的远程分支之间

  - `git merge [branch]`

    合并指定分支到当前分支

  - `git cherry-pick [commit]`

    选择一个commit ,合并进当前分支

  - `git branch -d [branch-name]`

    删除分支

  - `git push origin --delete [branch-name]`

    `git branch -dr [remote/branch]`

    删除远程分支

- 标签

  - `git tag`

    列出所有tag

  - `git tag [tag]`

    新建一个tag在之前commit

  - `git tag [tag] [commit]`

    新建一个tag在指定commit

  - `git tag -d [tag]`

    删除本地tag

  - `git push origin :refs/tags/[tagName]`

    删除远程tag

  - `git show [tag]`

    查看tag信息

  - `git push [remote] [tag]`

    提交指定tag

  - `git push [remote] --tags`

    提交所有tag

  - `git checkout -b [branch] [tag]`

    新建一个分支，指向每个tag

- **查看信息**

  - `git status`

    显示有变更的文件

  - `git log`

    显示当前分支的版本历史

  - `git log --stat`

    显示commit历史，以及每次commit发生变更的文件

  - `git log -S [keyword]`

    搜索提交历史，根据关键词

  - `git log --graph`

    用图表形式输出提交日志

  - `git log [tag] HEAD --pretty=format:%s`

    显示某个commit之后的所有变动，每个commit占据一行

  - `git log [tag] HEAD --grep feature`

    显示某个commit之后的所有变动，其“提交说明”必须符合搜索条件

  - `git log --follow [file]`

    `git whatchanged [file]`

    显示某个文件的版本历史，包括文件改名

  - `git log -p [file]`

    显示指定文件相关的每一次diff

  - `git log -5 --pretty --oneline`

    显示过去5次提交

  - `git shortlog -sn`

    显示所有提交过的用户，按提交次数排序

  - `git blame [file]`

    显示指定文件是什么人在什么时间修改过

  - `git diff` 

    显示暂存区和工作区的代码差异

  - `git diff --cached [file]`

    显示暂存区和上一个commit之间的差异

  - `git diff HEAD`

    显示工作区与当前分支最新commit之间的差异

  - `git diff [first-branch]...[second-branch]`

    显示两次提交之间的差异

  - `git diff --shortstat "@{0 day ago}"`

    显示今天你写了多少行代码

  - `git show [commit]`

    显示某次提交的元数据和内容变化

  - `git show --name-only [commit]`

    显示某次提交发生变化的文件

  - `git show [commit]:[filename]`

    显示某次提交时，某个文件的内容

  - `git reflog`

    显示当前分支的最近几次提交

  - `git rebase [branch]`

    从本地master拉取代码更新当前分支：branch 一般为master

  - `git rebase -i`

    压缩历史

- 远程同步

  - `git remote update` 

    更新远程仓库

  - `git fetch [remote]`

    下载远程仓库的所有变动

  - `git remote -v`

    显示所有远程仓库

  - `git remote show [remote]`

    显示某个远程仓库的信息

  - `git remote add [shortname] [url]`

    增加一个新的的远程仓库，并命名

  - `git pull [remote] [branch]`

    取回远程仓库的变化，并与本地分支合并

  - `git push [remote] [branch]`

    上传本地指定分支到远程仓库

  - `git push [remote] --force`

    强行推送当前分支到远程仓库，即使有冲突

  - `git push [remote] -all`

    推送所有分支到远程仓库

  - 

- 撤销
  - `git checkout [file]`

    恢复暂存区的指定文件到工作区

  - `git checkout [commit] [file]`

    恢复某个commit的指定文件到暂存区和工作区

  - `git checkout .`

    恢复暂存区的所有文件到工作区

  - `git reset [file]`

    重置暂存区的指定文件，与上一次commit保持一致，但工作区不变

  - `git reset --hard`

    重置暂存区与工作区，与上一次commit保持一致

  - `git reset [commit]`

    重置当前分支的指针为指定commit，同时重置暂存区，但工作区不变

  - `git reset --hard [commit]`

    重置当前分支的HEAD为指定commit，同时重置暂存区和工作区，与指定commit一致

  - `git reset --keep [commit]`

    重置当前HEAD为指定commit，但保持暂存区和工作区不变

  - `git revert [commit]`
  
    新建一个commit，用来撤销指定commit
    后者的所有变化都将被前者抵消，并且应用到当前分支

  - `git stash`
    `git stash pop`

    暂时将未提交的变化移除，稍后再移入

- 其他
    - `git archive`

    生成一个可供发布的压缩包

- 

- 

- 

摘：

[https://www.jianshu.com/p/46ffff059092]: 

