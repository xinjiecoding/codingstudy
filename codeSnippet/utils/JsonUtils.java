
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

/**
 * Json操作工具类
 *
 */
@SuppressWarnings("all")
public class JsonUtils {

	/**
	 * json字符串解析为对象
	 * @param requestJson
	 * @param className
	 * @return
	 */
	public static Object decodeJsonStr(String requestJson, Class className){
		Object requestMsg = JSON.parseObject(requestJson, className);
		return requestMsg;
	}
	
	/**
	 * json对象解析为对象
	 * @param requestJson
	 * @param className
	 * @return
	 */
	public static Object decodeJsonObj(JSON object, Class className){
		Object requestObject = JSON.toJavaObject(object, className);
		return requestObject;
	}
	
	
	/**
	 * json集合解析为List对象集合
	 * @param jsonArray
	 * @param className
	 * @return
	 */
	public static List<Object> decodeJsonArray(JSONArray jsonArray, Class className){
		List<Object> objects = new ArrayList<Object>();
		if(jsonArray != null){
			for(int a = 0; a < jsonArray.size(); a++){
				JSON jsonArraySubObject = (JSON)jsonArray.get(a);
				Object object = decodeJsonObj(jsonArraySubObject, className);
				objects.add(object);
			}
		}
		return objects;
	}
	/**
	 * 修改方法在使用时不需要强转类型
	 * @param requestJson
	 * @param className
	 * @return
	 */
	public static <T> T decodeJsonStrNew(String requestJson, Class<T> className){
		T requestMsg = JSON.parseObject(requestJson, className);
		return requestMsg;
	}
	/**
	 * 修改方法在使用时不需要强转类型
	 * @param requestJson
	 * @param className
	 * @return
	 */
	public static <T> T decodeJsonObjNew(JSON object, Class<T> className){
		T requestObject = JSON.toJavaObject(object, className);
		return requestObject;
	}
	/**
	 * 修改方法在使用时不需要强转类型
	 * @param requestJson
	 * @param className
	 * @return
	 */
	public static <T> List<T> decodeJsonArrayNew(JSONArray jsonArray, Class<T> className){
		List<T> objects = new ArrayList<T>();
		if(jsonArray != null){
			for(int a = 0; a < jsonArray.size(); a++){
				JSON jsonArraySubObject = (JSON)jsonArray.get(a);
				T object = decodeJsonObjNew(jsonArraySubObject, className);
				objects.add(object);
			}
		}
		return objects;
	}
}
