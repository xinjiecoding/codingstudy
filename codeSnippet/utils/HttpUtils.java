

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUtils{
    public static String doPostHttp(String url, String param) {
		String strReturn = "";
		HttpURLConnection httpConnection;
		try {
            URL realUrl = new URL(url);
        // 1、打开连接
            // 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)  
            httpConnection = (HttpURLConnection) realUrl.openConnection();// 此时cnnection只是为一个连接对象,待连接中
            // 设置请求方式为post  
            httpConnection.setRequestMethod("POST");
            // 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)  
            httpConnection.setDoOutput(true);
            // 设置连接输入流为true  
            httpConnection.setDoInput(true);
            // 设置是否使用缓存 Post 请求不能使用缓存 
			httpConnection.setUseCaches(false);
			// 设置连接主机超时（单位：毫秒）
            httpConnection.setConnectTimeout(20000);
            // 设置从主机读取数据超时（单位：毫秒）
            httpConnection.setReadTimeout(300000);
            // 设置请求头
			httpConnection.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8");
            httpConnection.setAllowUserInteraction(true);
            // 设置该HttpURLConnection实例是否自动执行重定向  
            connection.setInstanceFollowRedirects(true);  
            // 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)  
			httpConnection.connect();
        // 2、发送数据
            // 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)  
            OutputStreamWriter dataout = new OutputStreamWriter(connection.getOutputStream(), "utf-8");
            // 将参数输出到连接  
            //dataout.writeBytes(string);  
            dataout.write(string);
            // 输出完成后刷新并关闭流  
            dataout.flush();  
            dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)   

		// 3、返回数据
			InputStreamReader inputStreamReader = new InputStreamReader(httpConnection.getInputStream(), "UTF-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String inputLine = "";
            // 用来存储响应数据
            StringBuffer inputLines = new StringBuffer();
            // 循环读取流,若不到结尾处 
			while ((inputLine = bufferedReader.readLine()) != null) {
				inputLines.append(inputLine);
			}
			inputStreamReader.close();
			bufferedReader.close();
		// 4、关闭连接
			httpConnection.disconnect();
			strReturn = inputLines.toString();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return strReturn.toString();
	}
}