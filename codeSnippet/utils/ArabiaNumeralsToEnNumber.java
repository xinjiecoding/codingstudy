import java.util.Scanner;

/**
 * 摘自: https://blog.csdn.net/yf224/article/details/70790451
 * 阿拉伯数字转英文
 *  最高 999
 */
public class ArabiaNumeralsToEnNumber {
    public static String Bits[]={"ONE","TWO","THREE","FOUR","FIVE","SIX","SEVEN","EIGHT","NINE","TEN"};
    public static String Teens[]={"ELEVEN","TWELVE","THIRTEEN","FOURTEEN","FIFTEEN","SIXTEEN","SEVENTEEN","EIGHTEEN","NINETEEN"};
    public static String Ties[]={"TWENTY","THIRTY","FORTY","FIFTY","SIXTY","SEVENTY","EIGHTY","NINETY"};

    public static void main(String[] args) {
        Scanner  s=new Scanner(System.in);
        int n=0;
        while(n!=-1){
            System.out.println("请输入数字：");
            n=s.nextInt();
            if(n>999||n<0){
                return;
            }
            String a=toenglish(n);
            System.out.println(a);
        }
    }

    public static String toenglish(int n){
        if(n==0){
            return "ZERO";
        }
        StringBuffer sb=new StringBuffer();
        if(n>=100){
            sb.append(toHundred(n));
            if(n%100!=0){
                sb.append(" AND ");
            }
            n-=(n/100)*100;
        }
        boolean isthan20=false;
        if(n>=20){
            isthan20=true;
            sb.append(toTies(n));
            n-=(n/10)*10;
        }
        if(!isthan20&&n>10){
            sb.append(toTeens(n));
            n=0;
        }
        if(n>0){
            if(!isthan20){
                sb.append(" ");
            }
            sb.append(toBits(n));
        }
        return sb.toString();
    }
    public static String toHundred(int n){
        int hundred=n/100;
        return Bits[hundred-1]+" HUNDRED";
    }
    public static String toTies(int n){
        int ties=n/10-1;
        return Ties[ties-1]+" ";
    }
    public static String toTeens(int n){
        return Teens[n-11]+" ";
    }
    public static String toBits(int n){
        return Bits[n-1]+" ";
    }
}